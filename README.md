# Paperclip - TinyPNG

If you are using excellent [Paperclip gem](https://github.com/thoughtbot/paperclip) for image uploads in your Ruby on Rails application, and want your images to be optimized in terms of size for the web without losing image quality, you have arrived at the right place. Enter Paperclip-TinyPNG !

Paperclip-TinyPNG is a Paperclip preprocessor that uses the amazing [TinyPNG app](https://tinypng.com/) to optimize your images.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'paperclip_optimal_compressor'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install paperclip_optimal_compressor

## Usage

Once the gem has been added to your project, just a couple more steps before you can cruise head:

- Obtain an API Key from TinyPNG - [TinyPNG Developer API](https://tinypng.com/developers)

- Add a initializer file `config/initializers/tinypng.rb` to setup the API key.

```ruby
  Paperclip::Tinypng.api_key = 'your-api-key'
```

- In Paperclip enabled model, specify the TinyPNG preprocessor. 

```ruby
  has_attached_file :avatar, styles: {thumbnail: "60x60#"}, processors: [:tinypng]
```

If you have no `styles` defined add a dummy style.

```ruby
  has_attached_file :avatar, :styles => {original: 'unused_option'}, processors: [:tinypng]
```

Restart the app and you are good to go !

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

