# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'paperclip_optimal_compressor/version'

Gem::Specification.new do |spec|
  spec.name          = "paperclip_optimal_compressor"
  spec.version       = PaperclipOptimalCompressor::VERSION
  spec.authors       = ["Unnikrishnan KP"]
  spec.email         = ["unni@zenleaf.in"]

  spec.summary       = %q{Optimize the uploaded images in terms of their size without compromising image quality visibly}
  spec.description   = %q{It is a paperclip preprocessor that uses the amazing TinyPNG app (https://tinypng.com/) to optimize your images}
  spec.homepage      = "https://gitlab.com/unni.tallman/paperclip_optimal_compressor"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
end
